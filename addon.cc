/*******************************************************************************
 * Mash Node.js native module
 * Developed by The Centre for Genomic Pathogen Surveillance (CGPS)
 *              <http://www.pathogensurveillance.net/>
 * MIT License <https://gitlab.com/cgps/mash-node-native/blob/master/LICENSE.md>
 ******************************************************************************/

#include <node.h>
#include <nan.h>

#include "sync.h"
#include "async.h"
#include "allMatchesSync.h"

// using namespace v8;
// using namespace Nan;
using v8::FunctionTemplate;
using v8::String;
using Nan::GetFunction;
using Nan::New;
using Nan::Set;

// Expose synchronous and asynchronous access `closestMatch()` function
NAN_MODULE_INIT(InitAll) {
  Set(target, New<String>("closestMatchSync").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(ClosestMatchSync)).ToLocalChecked());

  Set(target, New<String>("closestMatchAsync").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(ClosestMatchAsync)).ToLocalChecked());

  Set(target, New<String>("allMatchesSync").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(AllMatchesSync)).ToLocalChecked());
}

NODE_MODULE(addon, InitAll)
