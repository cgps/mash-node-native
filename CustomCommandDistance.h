
#include "CommandDistance.h"

using namespace::std;

class CustomCommandDistance : public CommandDistance
{
public:

    CustomCommandDistance();

    int run() const; // override

    void writeCustomOutput(CompareOutput * output, bool table) const;

    vector<CompareOutput::PairOutput> getAllMatches(vector<string> filenames) const;

    // mutable CompareOutput::PairOutput* pairs;
    // mutable uint64_t pairCount;

    // mutable std::vector<double> outputResult;

    mutable vector<CompareOutput::PairOutput> outputPairs;
};
