#include <iostream>
#include <vector>

#include "closestMatch.h"
#include "CustomCommandDistance.h"

using namespace::std;

vector<double> closestMatch(vector<string> filenames) {
  CustomCommandDistance cmd;
  vector<CommandDistance::CompareOutput::PairOutput> outputPairs = cmd.getAllMatches(filenames);

  uint64_t t = 0;
  for (uint64_t k = 0; k < cmd.outputPairs.size(); k++)
  {
    if (outputPairs[k].distance < outputPairs[t].distance)
    {
      t = k;
    }
    else if (outputPairs[k].distance == outputPairs[t].distance)
    {
      if (outputPairs[k].pValue < outputPairs[t].pValue)
      {
        t = k;
      }
    }
  }

  // create a new JS array
  vector<double> results;
  results.push_back(t);
  results.push_back(outputPairs[t].distance);
  results.push_back(outputPairs[t].pValue);
  results.push_back(outputPairs[t].numer);
  results.push_back(outputPairs[t].denom);
  return results;
}
