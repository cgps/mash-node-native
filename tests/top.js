/* eslint no-console: 0 */
/* eslint no-unused-vars: 0 */

const fs = require('fs');
const path = require('path');
const mash = require('../index');

const sketchFilePath = process.argv[2];
const fastaFilePath = process.argv[3];

function getAllMatches(n = 20) {
  const matches = mash.allMatchesSync(sketchFilePath, fastaFilePath);
  const sorted = matches.sort((a, b) => {
    if (a.mashDistance !== b.mashDistance) {
      return a.mashDistance - b.mashDistance;
    } else if (a.pValue !== b.pValue) {
      return a.pValue - b.pValue;
    } else {
      return a.referenceIndex - b.referenceIndex;
    }
  });
  const topMatches = [];

  let index = 0;
  const dist = sorted[0].mashDistance;
  while (index < n || sorted[index].mashDistance === dist) {
    topMatches.push(sorted[index]);

    index += 1;
  }

  return topMatches;
}

function printCells(cells) {
  const vals = cells.map(
    ([ value, length ]) => `${value}${' '.repeat(Math.max(length, value.length) - value.length)}`
  );
  console.log('', vals.join('| '));
}

function printAsTable(headerCells, rows) {
  printCells(headerCells);

  console.log(
    headerCells.map(([ , length ]) => '-'.repeat(length + 1))
    .join('|')
  );

  for (const row of rows) {
    printCells(
      row.map((cell, index) => [ cell, headerCells[index][1] ])
    );
  }
}

Promise.all([
  getAllMatches(),
])
.then(([ topMatches ]) => {
  printAsTable(
    [
      [ 'Index', 8 ],
      [ 'Distance', 22 ],
      [ 'P-Value', 22 ],
      [ 'Hashes', 8 ],
      [ 'Hashes', 8 ],
    ],
    topMatches.map(match => {
      const { referenceIndex, mashDistance, pValue, matchingHashes, allHashes } = match;
      console.log(match);
      return [
        referenceIndex.toString(),
        mashDistance.toString(),
        pValue.toString(),
        matchingHashes.toString(),
        allHashes.toString(),
      ];
    })
  );
});
