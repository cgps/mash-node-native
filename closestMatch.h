#ifndef _MASH_CLOSEST_MATCH_H_
#define _MASH_CLOSEST_MATCH_H_

#include <iostream>
#include <vector>

std::vector<double> closestMatch(std::vector<std::string> filenames);

#endif
