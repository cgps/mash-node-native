#!/bin/sh
set -e

apk add --no-cache --virtual build-deps \
    autoconf \
    automake \
    curl \
    file \
    g++ \
    gcc \
    git \
    gsl-dev \
    libtool \
    linux-headers \
    make \
    tar \
    zlib-dev
