#include <node.h>
#include <nan.h>
#include <functional>

#include "CustomCommandDistance.h"
#include "exchange.h"

using namespace::std;
using namespace v8;
using namespace Nan;

// void getAllMatches(const FunctionCallbackInfo<Value>& args) {
//   Isolate* isolate = Isolate::GetCurrent();
//   HandleScope scope(isolate);
//
//   // Make sure there is an argument.
//   if (args.Length() != 1) {
//     isolate->ThrowException(
//       Exception::TypeError(
//         String::NewFromUtf8(isolate, "First argument must be an array of filenames")
//       )
//     );
//     return;
//   }
//
//   // Make sure it's an array.
//   if (! args[0]->IsArray()) {
//     isolate->ThrowException(
//       Exception::TypeError(
//         String::NewFromUtf8(isolate, "First argument must be an array of filenames")
//       )
//     );
//     return;
//   }
//
//   // Unpack JS array into a std::vector
//   vector<string> filenames;
//   Local<Array> input = Local<Array>::Cast(args[0]);
//   unsigned int numberOfFiles = input->Length();
//   for (unsigned int i = 0; i < numberOfFiles; i++) {
//     v8::String::Utf8Value param(input->Get(i)->ToString());
//     string from = std::string(*param);
//     filenames.push_back(from);
//   }
//
//   CustomCommandDistance cmd;
//   vector<CommandDistance::CompareOutput::PairOutput> outputPairs = cmd.getAllMatches(filenames);
//
//   // create a new JS array
//   Local<Array> result = Array::New(isolate);
//
//   for (uint64_t k = 0; k < cmd.outputPairs.size(); k++)
//   {
//     Local<Array> row = Array::New(isolate);
//     row->Set(0, Number::New(isolate, outputPairs[k].distance));
//     row->Set(1, Number::New(isolate, outputPairs[k].pValue));
//     row->Set(2, Number::New(isolate, outputPairs[k].numer));
//     row->Set(3, Number::New(isolate, outputPairs[k].denom));
//     result->Set(k, row);
//   }
//
//   args.GetReturnValue().Set(result);
// }

// void getClosestMatch(const FunctionCallbackInfo<Value>& args) {
//   Isolate* isolate = Isolate::GetCurrent();
//   HandleScope scope(isolate);
//
//   // Make sure there is an argument.
//   if (args.Length() != 2) {
//     isolate->ThrowException(
//       Exception::TypeError(
//         String::NewFromUtf8(isolate, "Invalid use. Expected exactly two arguments")
//       )
//     );
//     return;
//   }
//
//   // Make sure args are strings.
//   if (! args[0]->IsString()) {
//     isolate->ThrowException(
//       Exception::TypeError(
//         String::NewFromUtf8(isolate, "First argument must be a string")
//       )
//     );
//     return;
//   }
//   if (! args[1]->IsString()) {
//     isolate->ThrowException(
//       Exception::TypeError(
//         String::NewFromUtf8(isolate, "Second argument must be a string")
//       )
//     );
//     return;
//   }
//
//   // Unpack JS array into a std::vector
//   vector<string> filenames;
//   for (unsigned int i = 0; i < 2; i++) {
//     v8::String::Utf8Value param(args[i]->ToString());
//     string from = std::string(*param);
//     filenames.push_back(from);
//   }
//
//   CustomCommandDistance cmd;
//   vector<CommandDistance::CompareOutput::PairOutput> outputPairs = cmd.getAllMatches(filenames);
//
//   uint64_t t = 0;
//   for (uint64_t k = 0; k < cmd.outputPairs.size(); k++)
//   {
//     if (outputPairs[k].distance < outputPairs[t].distance)
//     {
//       t = k;
//     }
//     else if (outputPairs[k].distance == outputPairs[t].distance)
//     {
//       if (outputPairs[k].pValue < outputPairs[t].pValue)
//       {
//         t = k;
//       }
//     }
//   }
//
//   // create a new JS array
//   Local<Array> result = Array::New(isolate);
//   result->Set(0, Number::New(isolate, t + 1));
//   result->Set(1, Number::New(isolate, outputPairs[t].distance));
//   result->Set(2, Number::New(isolate, outputPairs[t].pValue));
//   result->Set(3, Number::New(isolate, outputPairs[t].numer));
//   result->Set(4, Number::New(isolate, outputPairs[t].denom));
//
//   args.GetReturnValue().Set(result);
//
//   // cout << "[[test]] 5" << endl;
// }

int executeClosestMatchCommand(vector<string> filenames) {
  CustomCommandDistance cmd;
  vector<CommandDistance::CompareOutput::PairOutput> outputPairs = cmd.getAllMatches(filenames);

  uint64_t t = 0;
  for (uint64_t k = 0; k < cmd.outputPairs.size(); k++)
  {
    if (outputPairs[k].distance < outputPairs[t].distance)
    {
      t = k;
    }
    else if (outputPairs[k].distance == outputPairs[t].distance)
    {
      if (outputPairs[k].pValue < outputPairs[t].pValue)
      {
        t = k;
      }
    }
  }

  // create a new JS array
  // Local<Array> result = Array::New(isolate);
  // result->Set(0, Number::New(isolate, t + 1));
  // result->Set(1, Number::New(isolate, outputPairs[t].distance));
  // result->Set(2, Number::New(isolate, outputPairs[t].pValue));
  // result->Set(3, Number::New(isolate, outputPairs[t].numer));
  // result->Set(4, Number::New(isolate, outputPairs[t].denom));
  // return result;

  return t + 1;
}

class ClosestMatchWorker : public AsyncWorker {
  public:
    ClosestMatchWorker(Callback *callback, vector<string> filenames)
    : AsyncWorker(callback), filenames(filenames) {}

    ~ClosestMatchWorker() {}

    void Execute () {
      // Exchange x(
      //   [&](void * data) {
      //     primes.push_back(*((int *) data));
      //   }
      // );
      //
      // getClosestMatchAsync(under, (void*)&x);
      index = executeClosestMatchCommand(filenames);
    }

    // We have the results, and we're back in the event loop.
    void HandleOKCallback () {
      Nan::HandleScope scope;

      // v8::Local<v8::Array> results = New<v8::Array>(primes.size());
      // int i = 0;
      // for_each(primes.begin(), primes.end(),
      //   [&](int value) {
      //     Nan::Set(results, i, New<v8::Number>(value));
      //     i++;
      // });

      Local<Value> argv[] = {
        Null(),
        New<Number>(index)
      };

      callback->Call(2, argv);
    }

  private:
    vector<string> filenames;
    Local<Array> results;
    int index;
};

NAN_METHOD(ClosestMatch) {
  vector<string> filenames;
  for (unsigned int i = 0; i < 2; i++) {
    v8::String::Utf8Value v8String(info[i]->ToString());
    // v8::String v8String = To<v8::String>(info[i]).ToLocalChecked();
    string stlString = std::string(*v8String);
    filenames.push_back(stlString);
  }
  Callback *callback = new Callback(info[2].As<Function>());
  AsyncQueueWorker(new ClosestMatchWorker(callback, filenames));
}

NAN_MODULE_INIT(Init) {
  Nan::Set(target, New<String>("closestMatchAsync").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(ClosestMatch)).ToLocalChecked());
}

NODE_MODULE(addon, Init)

// void init(Handle<Object> exports) {
//   NODE_SET_METHOD(exports, "getAllMatches", getAllMatches);
//   NODE_SET_METHOD(exports, "getClosestMatch", getClosestMatch);
// }
//
// NODE_MODULE(mash, init)
